function semivariogram = exploreSemivariogram(residIm,coord,dist_cutoff,nbins,robust,standardizeResiduals,numVoxelsToUse)
% exploreSemivariogram Estimate the semivariogram for spatially arranged
%                      data
%
% semivariogram = exploreSemivariogram(residIm,coord,dist_cutoff,nbins,...
%                   [robust],[standardizeResiduals],[numVoxelsToUse])
%
% INPUTS:
% residIm               Image of model residuals. One biomarker (voxel)
%                       per column. Number of rows equal to number of
%                       subject-visits.
% coord                 struct with fields X, Y, Z, each of which is a
%                       numBiomarkers-by-1 vector containing the X, Y, Z
%                       coordinates of the biomarkers
% dist_cutoff           Maximum distance for estimating semivariogram
% nbins                 Number of distance bins to use for semivariogram
% robust                OPTIONAL: If true, Cressie's robust estimator will
%                       be used. Defaults to false.
% standardizeResiduals  OPTIONAL: If true, each biomarker (voxel) residual 
%                       will be standardized to have 0 mean and 1 variance
%                       across subject-visits. Defaults to false.
% numVoxelsToUse        OPTIONAL: Will randomly select numVoxels to use in
%                       semivariogram estimation. Defaults to
%                       numBiomarkers, meaning that all voxels will be
%                       used.
%
%   See the paper:
%   Cressie, N., Hawkins, D.M., 1980. Robust estimation of the variogram.
%   Journal of the International Association of Mathematical Geology 12,
%   115-125.
%
% This file is part of Progression Score Model Toolkit.
%
% Progression Score Model Toolkit is distributed under the terms of GNU
% Lesser General Public License, version 3 (GPLv3).
%
% Progression Score Model Toolkit is free software: you can redistribute it
% and/or modify it under the terms of the GNU Lesser General Public License
% as published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% Progression Score Model Toolkit is distributed in the hope that it will
% be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.
%
% You should have received a copy of the GNU Lesser General Public License
% along with Progression Score Model Toolkit. If not, see
% <http://www.gnu.org/licenses>.
%
%
% Progression Score Model Toolkit was made by Murat Bilgel and is
% supported, in part, by the National Institute on Aging, a U.S. Federal
% Government research institute, while Murat Bilgel was a participant in
% the NIH Graduate Partnerships Program.
%
% For any code or method related questions, contact Murat Bilgel at
% mbilgel1@jhu.edu.
%

numSubjVisits = size(residIm,1);
numBiomarkers = size(residIm,2);

if nargin<7
    % use all voxels
    numVoxelsToUse = numBiomarkers;
elseif numVoxelsToUse > numBiomarkers
    warning('Number of voxels to use in semivariogram calculation cannot be greater than number of voxels! Setting it to number of voxels');
    numVoxelsToUse = numBiomarkers;
end

if nargin<6
    standardizeResiduals = false;
end

if nargin<5
    robust = true;
end

breaks = linspace(0,dist_cutoff,nbins+1);
sqbreaks = breaks.^2;
sqdist_cutoff = dist_cutoff^2;
nObs_binned = zeros(nbins,1);
val_binned = zeros(nbins,1);
dist_binned = breaks;
center_binned = (dist_binned(1:nbins) + dist_binned(2:nbins+1))/2;

if standardizeResiduals
    % Standardize residuals
    residMean = mean(residIm);
    residSD = std(residIm);
    residIm = bsxfun(@rdivide,bsxfun(@minus,residIm,residMean),residSD);
end

selectedIdx = sort(randsample(1:numBiomarkers,numVoxelsToUse));

for subid=1:length(selectedIdx)
    id = selectedIdx(subid);
    voxid = selectedIdx(subid:end);
    sqdists = (coord.X(id)-coord.X(voxid)).^2 + ...
              (coord.Y(id)-coord.Y(voxid)).^2 + ...
              (coord.Z(id)-coord.Z(voxid)).^2;
    proximityMask = sqdists < sqdist_cutoff;
    if any(proximityMask)
        [nObs_result,bin] = histc(sqdists(proximityMask),sqbreaks);

        if robust
            % Cressie's robust estimator
            val = sqrt(abs(bsxfun(@minus,residIm(:,voxid(proximityMask)),residIm(:,id))));
        else
            val = bsxfun(@minus,residIm(:,voxid(proximityMask)),residIm(:,id)).^2;
        end
        
        tmp1 = repmat(bin(bin>0)',numSubjVisits,1);
        tmp2 = val(:,bin>0);
        val_result = zeros(nbins,1);
        validRange = 1:max(bin(bin>0));
        val_result(validRange) = accumarray(tmp1(:),tmp2(:));
    end
    
    nObs_result = nObs_result(:);
    nObs_binned = nObs_result(1:nbins) + nObs_binned;
    val_binned = val_result + val_binned;
end

semivariogram.x = center_binned;
if robust
    semivariogram.y = 0.5 * (val_binned ./ (numSubjVisits*nObs_binned)).^4 ./ (0.457 + 0.494./(numSubjVisits*nObs_binned));
else
    semivariogram.y = val_binned ./ (2*numSubjVisits*nObs_binned);
end

semivariogram.n = nObs_binned;

semivariogram.standardized = standardizeResiduals;

figure, plot(semivariogram.x,semivariogram.y,'.-'); title('Semivariogram'); xlabel('Distance'); ylabel('1 - correlation');