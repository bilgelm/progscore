function model = fitLinearDPS_EM_nocorr(da,y,dx_normal,...
                 tol,maxIterations,age_colname,dx_colname,...
                 doMultiLambda,isdiagonal_V,model_init,verbose)
% fitLinearDPS_EM_nocorr  Fit the linear progression score model, assuming
%                         all biomarker residuals are independent
%                         Model:
%                         s_{ij} = \alpha_i t_{ij} + \beta_i
%                         y_{ijk} = a_k s_{ij} + b_k + \epsilon_{ijk}
%                         (\alpha_i,\beta_i) bivariate normally 
%                         distributed with mean ubar and variance V
%                         \epsilon_{ijk} normally distributed
%                         with mean 0 and variance \lambda_k^2
%
%   See the accompanying paper:
%   Murat Bilgel, Jerry L. Prince, Dean F. Wong, Susan M. Resnick, Bruno M.
%   Jedynak, "A multivariate nonlinear mixed effects model for longitudinal
%   image analysis: Application to amyloid imaging"
%   DOI: 10.1016/j.neuroimage.2016.04.001
%
% model = fitLinearDPS_EM_nocorr(da,y,dx_normal,...
%                 tol,maxIterations,age_colname,dx_colname,...
%                 doMultiLambda,isdiagonal_V,[model_init],[verbose])
%
% INPUTS:
% da            dataset containing ID, visit number, age, diagnosis. Column
%               containing IDs must be titled "subjectIDdps". IDs must be
%               in consecutive order, starting with 1 and ending with
%               number of subjects. Column containing visit numbers must be
%               titled "visitdps". Within each subject, visit numbers must
%               be in consecutive order, starting with 1 and ending with
%               number of visits. See prepareDPS.m for preprocessing data
%               sets to satisfy these requirements.
% y             matrix containing biomarker measurements (one biomarker 
%               per column). Must have equal number of rows as da.
%               Rows of da and y must be in correspondance (i.e. must
%               correspond to the same subject and visit)
% dx_normal     diagnosis value in da corresponding to controls/normals
% tol           convergence tolerance for EM algorithm
% maxIterations maximum number of iterations for EM algorithm
% age_colname   name of column in da containing age information
% dx_colname    name of column in da containing diagnosis information
% doMultiLambda set to TRUE if residuals are allowed to have different
%               standard deviations \lambda_k across biomarkers.
%               Set to FALSE if all residuals have the same standard
%               deviation \lambda across biomarkers
% isdiagonal_V  set to TRUE if the variance V of (\alpha_i,\beta_i) is
%               assumed to be diagonal (i.e. \alpha and \beta are
%               independent).
% model_init    OPTIONAL: model for initializing a_k, b_k, \lambda_k, 
%               \alpha_i, \beta_i
% verbose       OPTIONAL: logical indicating whether info messages should
%               be displayed
%
% See also prepareDPS, fitLinearDPS_EM_spatial.
%
%
% This file is part of Progression Score Model Toolkit.
%
% Progression Score Model Toolkit is distributed under the terms of GNU
% Lesser General Public License, version 3 (GPLv3).
%
% Progression Score Model Toolkit is free software: you can redistribute it
% and/or modify it under the terms of the GNU Lesser General Public License
% as published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% Progression Score Model Toolkit is distributed in the hope that it will
% be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.
%
% You should have received a copy of the GNU Lesser General Public License
% along with Progression Score Model Toolkit. If not, see
% <http://www.gnu.org/licenses>.
%
%
% Progression Score Model Toolkit was made by Murat Bilgel and is
% supported, in part, by the National Institute on Aging, a U.S. Federal
% Government research institute, while Murat Bilgel was a participant in
% the NIH Graduate Partnerships Program.
%
% For any code or method related questions, contact Murat Bilgel at
% mbilgel1@jhu.edu.
%

% Extract age and diagnosis data
age = double(da(:,age_colname));
dx = double(da(:,dx_colname));

numSubjects = length(unique(da.subjectIDdps));
numBiomarkers = size(y,2);
numVisitsPerSubject = accumarray(da.subjectIDdps,ones(length(da.subjectIDdps),1));
numSubjVisits = sum(numVisitsPerSubject);

if nargin<11
    verbose = true;
end

% if an initial model is provided, it will be used for parameter
% initialization; otherwise a simple initialization is used
if nargin<10 || isempty(model_init)
    % initialize parameters
    a = ones(numBiomarkers,1);
    b = zeros(numBiomarkers,1);
    lambda = ones(numBiomarkers,1);
    
    ubar = [1;0];
    V = eye(2);
    
    alpha = normrnd(1,1,numSubjects,1);
    beta = normrnd(0,1,numSubjects,1);
    
    loglik = -Inf;
else
    % use provided model to initialize parameters
    a = model_init.a;
    b = model_init.b;
    lambda = model_init.lambda;
    
    alpha = model_init.alpha;
    beta = model_init.beta;
    
    if isfield(model_init,'ubar') && isfield(model_init,'V')
        % ubar is what is referred to as \mathbf{m} in paper
        ubar = model_init.ubar;
        V = model_init.V;
        if isdiagonal_V
            V(~diag(true(2,1))) = 0;
        end
    else
        ubar = [mean(alpha); mean(beta)];

        if isdiagonal_V
            V = [var(alpha) 0; 0 var(beta)];
        else
            V = cov(alpha,beta);
        end
    end
    
    if isfield(model_init,'loglik')
        loglik = model_init.loglik(end);
    else
        loglik = -Inf;
    end
end

% ysum is \sum_{i,j} \mathbf{y}_{ij}
ysum = sum(y,1)';
% ybar is \frac{1}{\sum_i v_i} \sum_{i,j} \mathbf{y}_{ij}
ybar = mean(y,1)';

%%
if ~isdiagonal_V
    % Prior covariance V is 2-by-2 and off-diagonals are allowed to be
    % non-zero: V is characterized by 3 parameters
    % use log Cholesky decomposition to parameterize V as described in
    % section 2.1.2
    uppertri_V = triu(true(2));
    diag_V = diag(true(2,1));
    
    U = chol(V);
    U(diag_V) = log(U(diag_V));
    
    logv = U(uppertri_V);
else
    % Prior covariance V is assumed to be diagonal. To ensure positivity,
    % we use log-parameterization
    logv = log(diag(V));
end

%%
function V_expectation = calculateExpectationV(logv)
    % this function is used to perform the arg max in Eq.15
    % only the terms that depend on V in Q are retained -- see Eq.A.13
    UU = zeros(2);
    UU(uppertri_V) = logv;
    UU(diag_V) = exp(UU(diag_V));
    VV = UU'*UU;

    % term2 is (\hat{\mathbf{u}}'_i - \mathbf{m})^T V^{-1} (\hat{\mathbf{u}}'_i - \mathbf{m})
    term2 = 0;
    % term3 is Tr(V^{-1} \Sigma'_i)
    term3 = 0;
    for ii=1:numSubjects
        term2 = term2 + sum((UU'\([alpha(ii); beta(ii)]-ubar)).^2);
        term3 = term3 + Sigma_11(ii)*VV(2,2) - 2*Sigma_12(ii)*VV(1,2) + Sigma_22(ii)*VV(1,1);
    end
    
    V_expectation = -0.5*numSubjects*log(det(VV)) -0.5*term2 -0.5*term3/det(VV);
end

%% EM algorithm
itr = 1;
converged = 0;
loglik_list = zeros(maxIterations,1);

if verbose
    tic
end
while ~converged && itr<=maxIterations
    loglik_old = loglik;
    
    a_old = a;
    b_old = b;
    lambda_old = lambda;
    ubar_old = ubar;
    V_old = V;
    alpha_old = alpha;
    beta_old = beta;

    % E-step: Calculate \alpha'_i, \beta'_i, s'_{ij}
    Sigma_11 = zeros(numSubjects,1);
    Sigma_12 = zeros(numSubjects,1);
    Sigma_22 = zeros(numSubjects,1);
    
    % this corresponds to the trace term
    % Tr(\mathbf{q}_{ij} \mathbf{q}_{ij}^T \Sigma'_i)
    % in Eqns 12 and 13
    trace_term1 = 0;
    
    aRa = sum((a./lambda).^2);
    for i=1:numSubjects
        idx = da.subjectIDdps==i;
        yi = y(idx,:)';
        agei = age(idx);
        
        % invSi_mui is equal to 
        % \sum_j Z'_{ij}^T R'^{-1} (\mathbf{y}_{ij} - \mathbf{b}')
        % in Eq.10
        invSi_mui = sum( bsxfun(@times,[agei'; ones(1,numVisitsPerSubject(i))], ...
                         sum(bsxfun(@times,yi,a./lambda.^2))) ,2 ) ...
                   -sum(a.*b./lambda.^2) * [sum(agei); numVisitsPerSubject(i)];
        
        detV = det(V);
        
        % Covariance matrix \Sigma'_i as given below Eq.10
        Sigmai = [aRa*numVisitsPerSubject(i)+V(1,1)/detV -aRa*sum(agei)+V(1,2)/detV; ...
                  -aRa*sum(agei)+V(1,2)/detV aRa*sum(agei.^2)+V(2,2)/detV];
        Sigmai = Sigmai / det(Sigmai);
        
        Sigma_11(i) = Sigmai(1,1);
        Sigma_12(i) = Sigmai(1,2);
        Sigma_22(i) = Sigmai(2,2);
        
        trace_term1 = trace_term1 + Sigma_11(i)*sum(agei.^2) + 2*Sigma_12(i)*sum(agei) + Sigma_22(i)*numVisitsPerSubject(i);

        % \hat{\mathbf{u}}'_i as given in Eq.10
        ui = Sigmai*(invSi_mui + V\ubar); % vector containing \alpha'_i and \beta'_i
        
        alpha(i) = ui(1);
        beta(i) = ui(2);
    end
    % Progression scores s'_{ij}
    dps = repelem(alpha,numVisitsPerSubject) .* age + repelem(beta,numVisitsPerSubject);
    
    % sum_s is \sum_{i,j} s'_{ij}
    sum_s = sum(dps);
    % sum_ssq is \sum_{i,j} s'_{ij}^2
    sum_ssq = sum(dps.^2);
    % sum_ys is \sum_{i,j} \mathbf{y}_{ij} s'_{ij}
    sum_ys = sum(bsxfun(@times,y,dps),1)';
    
    % M-step
    a = (sum_ys - sum_s * ybar) / (sum_ssq + trace_term1 - sum_s^2 / numSubjVisits); % Eq.12
    b = ( ( sum_ssq + trace_term1 ) * ysum - sum_s * sum_ys) / ...
        (numSubjVisits * ( sum_ssq + trace_term1 ) - sum_s^2); % Eq.13
    
    y_pred = bsxfun(@plus,kron(a',dps),b');
    
    if doMultiLambda
        % Multi lambda: each biomarker is allowed to have a different variance
        lambda = sqrt( ( sum((y - y_pred).^2)' + trace_term1 * a.^2 ) / numSubjVisits ); % Eq.A.15
    else
        % Single lambda: all biomarkers have the same variance
        singleLambda = sqrt( ( sum((y(:) - y_pred(:)).^2) + trace_term1 * sum(a.^2) ) / (numSubjVisits * numBiomarkers));
        lambda = singleLambda * ones(numBiomarkers,1);
    end
    
    ubar = [mean(alpha); mean(beta)]; % Eq.14
    
    % Update V
    if isdiagonal_V
        V(1,1) = sum((alpha-ubar(1)).^2 + Sigma_11) / numSubjects;
        V(2,2) = sum((beta-ubar(2)).^2 + Sigma_22) / numSubjects;
    else
        [logv,objval] = fminsearch(@(x) -calculateExpectationV(x),logv); % Eq.15
        U = zeros(2);
        U(uppertri_V) = logv;
        U(diag_V) = exp(U(diag_V));

        V = U'*U;
    end
    
    % Compute incomplete loglikelihood with the new parameters
    detV = det(V);
    aRa = sum((a./lambda).^2);
    sum_yij_b = 0;
    sumlogdetSigmai = 0;
    sum_uiSigmaiui = 0;
    for i=1:numSubjects
        idx = da.subjectIDdps==i;
        yi = y(idx,:)';
        agei = age(idx);

        invSi_mui = sum( bsxfun(@times,[agei'; ones(1,numVisitsPerSubject(i))], ...
                     sum(bsxfun(@times,yi,a./lambda.^2))) ,2 ) ...
               -sum(a.*b./lambda.^2) * [sum(agei); numVisitsPerSubject(i)];

        Sigmai = [aRa*numVisitsPerSubject(i)+V(1,1)/detV -aRa*sum(agei)+V(1,2)/detV; ...
                  -aRa*sum(agei)+V(1,2)/detV aRa*sum(agei.^2)+V(2,2)/detV];
        Sigmai = Sigmai / det(Sigmai);

        sum_yij_b = sum_yij_b + sum(sum(bsxfun(@times,bsxfun(@minus,yi,b),1./lambda).^2));
        sumlogdetSigmai = sumlogdetSigmai + log(det(Sigmai));
        sum_uiSigmaiui = sum_uiSigmaiui + (invSi_mui + V\ubar)'*Sigmai*(invSi_mui + V\ubar);
    end

    % marginal loglikelihood as in Eq.A.17
    loglik = 0.5*( sumlogdetSigmai -numSubjects*log(detV) ...
                 -numSubjVisits*numBiomarkers*log(2*pi) ...
                 -numSubjVisits*2*sum(log(lambda)) ) ...
            -0.5*sum_yij_b ...
            -0.5*numSubjects*ubar'*(V\ubar) ...
            +0.5*sum_uiSigmaiui;
            
    if loglik<loglik_old
        warning(['Loglik decreased! ',num2str(loglik_old),' ',num2str(loglik)]);
        a = a_old;
        b = b_old;
        lambda = lambda_old;
        alpha = alpha_old;
        beta = beta_old;
        dps = repelem(alpha,numVisitsPerSubject) .* age + repelem(beta,numVisitsPerSubject);
        ubar = ubar_old;
        V = V_old;
        y_pred = bsxfun(@plus,kron(a',dps),b');
        loglik = loglik_old;
        loglik_list(itr) = loglik;
        converged = -1;
        break;
    end
    loglik_list(itr) = loglik;
    
    itr = itr+1;
    
    % Check for convergence
    if (abs(loglik/loglik_old - 1) < tol)
        converged = 1;
    end
    
    if verbose
        display([num2str(loglik_old),' ',num2str(loglik)]);
    end
end
if verbose
    toc
end

loglik_list(itr:end) = [];
if isempty(loglik_list)
    loglik_list = loglik;
end

%%
% Ensure that on average, PS increases with age
if ubar(1) < 0
    a = -a;
    alpha = -alpha;
    beta = -beta;
    dps = -dps;
    ubar = -ubar;
end

% Standardize PS as explained in section 2.4
mu = mean(dps(da.visitdps==1 & dx==dx_normal));
sdev = std(dps(da.visitdps==1 & dx==dx_normal));
if isnan(mu) || isnan(sdev)
    mu = mean(dps(da.visitdps==1));
    sdev = std(dps(da.visitdps==1));
end
alpha_scaled = alpha/sdev;
beta_scaled = (beta - mu)/sdev;
dps_scaled = repelem(alpha_scaled,numVisitsPerSubject) .* age + repelem(beta_scaled,numVisitsPerSubject);
a_scaled = sdev*a;
b_scaled = b + mu*a;
ubar_scaled = [ubar(1); ubar(2)-mu] / sdev;
V_scaled = V / sdev^2;

if isdiagonal_V
    nu_scaled = 0.5*log(diag(V_scaled));
else
    U = chol(V_scaled);
    U(diag_V) = log(U(diag_V));
    nu_scaled = U(uppertri_V);
end

posteriorVariance = zeros(2,2,numSubjects);
posteriorVariance(1,1,:) = Sigma_11 / sdev^2;
posteriorVariance(1,2,:) = Sigma_12 / sdev^2;
posteriorVariance(2,1,:) = Sigma_12 / sdev^2;
posteriorVariance(2,2,:) = Sigma_22 / sdev^2;

% Compute the variance of each PS value
dps_var = repelem(Sigma_11,numVisitsPerSubject).*age.^2 + ...
          2*repelem(Sigma_12,numVisitsPerSubject).*age + ...
          repelem(Sigma_22,numVisitsPerSubject);
dps_var_scaled = dps_var / sdev^2;

model.numParams = 2*numBiomarkers + numBiomarkers*doMultiLambda + 1*(~doMultiLambda) + 2 + 2*isdiagonal_V + 3*(~isdiagonal_V);

% Parameter estimates
model.a = a_scaled;
model.b = b_scaled;
model.lambda = lambda;
model.ubar = ubar_scaled;
model.V = V_scaled;
model.nu = nu_scaled;

% Values of \alpha and \beta that maximize the posterior
model.alpha = alpha_scaled;
model.beta = beta_scaled;
model.dps = dps_scaled;

% Predicted biomarker values (same dimension as y)
model.y_pred = y_pred;

model.loglik = loglik_list;

model.numIterations = itr-1;
model.converged = converged;

model.aic = 2*model.numParams - 2*loglik;

% Variance of the posterior for (\alpha,\beta)
model.posteriorVariance = posteriorVariance; % Var( \Alpha, \Beta | y, \theta )
% Variance of PS based on posterior for (\alpha,\beta)
model.dps_var = dps_var_scaled; % Var( S | y, \theta )

model.inputParams.dx_normal = dx_normal;
model.inputParams.tol = tol;
model.inputParams.maxIterations = maxIterations;
model.inputParams.age_colname = age_colname;
model.inputParams.dx_colname = dx_colname;
model.inputParams.isdiagonal_V = isdiagonal_V;
model.inputParams.doMultiLambda = doMultiLambda;
end