function [da_orig,images,origsubjlist,isbaseline,hdr,imdim,voxSize,mask,coord] =...
    prepareDPS_image(da_orig,subject_colname,visit_colname,age_colname,dx_colname,image_colname,maskpath,removeSingleVisits)
% prepareDPS_images Read in imaging data set and prepare for analysis with
%                   progression score model
%
% INPUTS:
% da_orig           dataset containing ID, visit number, age, diagnosis,
%                   biomarker measurements
% subject_colname   name of column in da_orig containing subject IDs
% visit_colname     name of column in da_orig containing visit numbers
% age_colname       name of column in da_orig containing age information
% dx_colname        name of column in da_orig containing diagnosis information
% image_colname     names of column in da_orig containing full path to
%                   images
% maskpath          full path to the binary mask specifying voxels of
%                   interest in the images. If you'd like to use all voxels
%                   in the image, specify maskpath as the empty string ''
% removeSingleVisits OPTIONAL: TRUE/FALSE indicating whether single visits 
%                    should beremoved (default=FALSE)
%
% OUTPUTS:
% da_orig           cleaned dataset
% y                 matrix containing biomarker measurements
% origsubjlist      list of original subject IDs
% isbaseline        vector of size equal to the number of rows of da_orig,
%                   indicating baseline visits
% hdr               a sample hdr file
% imdim             image dimensions
% voxSize           voxSize
% mask              binary mask specifying voxels of interest
% coord             struct with fields X, Y, Z specifying coordinates of
%                   each voxel
%
%
% This file is part of Progression Score Model Toolkit.
%
% Progression Score Model Toolkit is distributed under the terms of GNU
% Lesser General Public License, version 3 (GPLv3).
%
% Progression Score Model Toolkit is free software: you can redistribute it
% and/or modify it under the terms of the GNU Lesser General Public License
% as published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% Progression Score Model Toolkit is distributed in the hope that it will
% be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.
%
% You should have received a copy of the GNU Lesser General Public License
% along with Progression Score Model Toolkit. If not, see
% <http://www.gnu.org/licenses>.
%
%
% Progression Score Model Toolkit was made by Murat Bilgel and is
% supported, in part, by the National Institute on Aging, a U.S. Federal
% Government research institute, while Murat Bilgel was a participant in
% the NIH Graduate Partnerships Program.
%
% For any code or method related questions, contact Murat Bilgel at
% mbilgel1@jhu.edu.
%

% Check that all necessary columns exist in dataset
colnames = get(da_orig,'VarNames');
assert(strmatch(subject_colname,colnames) && ...
       strmatch(visit_colname,colnames) && ...
       strmatch(age_colname,colnames) && ...
       strmatch(dx_colname,colnames) && ...
       strmatch(image_colname,colnames));

if strmatch('subjectIDdps',colnames)
    error('Please rename the subjectIDdps column to something else before running this code.');
end
if strmatch('visitdps',colnames)
    error('Please rename the visitdps column to something else before running this code.');
end

if nargin<8
    removeSingleVisits = false;
end

% Retain necessary columns only
da_orig = da_orig(:,{subject_colname,visit_colname,age_colname,dx_colname,image_colname});

% Retain only the visits with subject identifier, age, and image data
ix = ismissing(da_orig,'StringTreatAsMissing',{'NaN','.','NA'});
da_orig = da_orig(~any(ix,2),:);

% Order data by subject and visit
da_orig = sortrows(da_orig,{subject_colname,visit_colname},'ascend');

% Remove subjects with single visit
if removeSingleVisits
    tbl = grpstats(da_orig(:,subject_colname),subject_colname);
    numVisitsBySubject = tbl.GroupCount;
    subjToRemove = double(tbl(numVisitsBySubject<2,subject_colname));
    for i=1:length(subjToRemove)
        da_orig(double(da_orig(:,subject_colname))==subjToRemove(i),:) = [];
    end
end

% Rename subjects and visits with consecutive integers to facilitate matrix
% indexing later
origsubjlist = unique(double(da_orig(:,subject_colname)));
numSubjects = length(origsubjlist);
isbaseline = false(size(da_orig,1),1);
da_orig.subjectIDdps = 0;
da_orig.visitdps = 0;
for s=1:numSubjects
    idx = double(da_orig(:,subject_colname))==origsubjlist(s);
    da_orig.subjectIDdps(idx) = s;
    isbaseline(find(idx,1)) = true;
    
    numVisit = length(da_orig(idx,visit_colname));
    numUniqueVisit = length(unique(da_orig(idx,visit_colname)));
    if numVisit~=numUniqueVisit
        error(['Visit numbers must be unique within subjects! Subject ',num2str(s),' violated this requirement!']);
    end
    
    % Rename visits with consecutive integers
    da_orig.visitdps(idx) = 1:numVisit;
end

image_paths = cellstr(da_orig(:,image_colname));
da_orig(:,image_colname) = [];

% Read in the mask image
if strcmp(maskpath,'')
    % If there is no mask specified, use all voxels in image
    % Read in first image
    im = load_untouch_nii(image_paths{1});
    imdim = size(im.img);
    mask = true(imdim);
    voxSize = im.hdr.dime.pixdim(2:4);
else
    tmp = load_untouch_nii(maskpath);
    mask = logical(tmp.img);
    imdim = size(mask);
    voxSize = tmp.hdr.dime.pixdim(2:4);
end

numVoxels = sum(mask(:));

[coordX,coordY,coordZ] = meshgrid(0:voxSize(2):voxSize(2)*(imdim(2)-1), ...
                                  0:voxSize(1):voxSize(1)*(imdim(1)-1), ...
                                  0:voxSize(3):voxSize(3)*(imdim(3)-1));

coord.X = coordX(mask);
coord.Y = coordY(mask);
coord.Z = coordZ(mask);

% Read in the images and store only the voxels within mask
tic
images = zeros(length(image_paths),numVoxels);
for i=1:length(image_paths)
    im = load_untouch_nii(image_paths{i});
    images(i,:) = im.img(mask);
end
toc

% Save one of the images for later (for saving results)
hdr = im;

