function model = fitLinearDPS_EM_spatial(da,y,dx_normal,...
                 tol,maxIterations,age_colname,dx_colname,...
                 model_nocorr,coord,rho_init,...
                 maxDistance_subset,isdiagonal_V,correlationFun,...
                 fixrho,fitRhoLambdaTogether,useNugget,w,verbose)
% fitLinearDPS_EM_spatial  Fit the linear progression score model with
%                          biomarker spatial correlations
%                          Model:
%                          s_{ij} = \alpha_i t_{ij} + \beta_i
%                          y_{ij} = a s_{ij} + b + \epsilon_{ij}
%                          (\alpha_i,\beta_i) bivariate normally 
%                          distributed with mean ubar and variance V
%                          \epsilon_{ij} normally distributed
%                          with mean 0 and variance R
%                          R = \lambda W C W, where lambda is a positive
%                          scalar, W is a diagonal matrix of fixed positive
%                          weights, C is a spatial correlation matrix 
%                          parameterized by rho > 0
%
%   See the accompanying paper:
%   Murat Bilgel, Jerry L. Prince, Dean F. Wong, Susan M. Resnick, Bruno M.
%   Jedynak, "A multivariate nonlinear mixed effects model for longitudinal
%   image analysis: Application to amyloid imaging"
%   DOI: 10.1016/j.neuroimage.2016.04.001
%
% model = fitLinearDPS_EM_spatial(da,y,dx_normal,...
%                 tol,maxIterations,age_colname,dx_colname,...
%                 model_nocorr,coord,rho_init,...
%                 maxDistance,maxDistance_subset,...
%                 isdiagonal_V,correlationFun,...
%                 [fixrho],[fitRhoLambdaTogether],[useNugget])
%
% INPUTS:
% da            dataset containing ID, visit number, age, diagnosis. Column
%               containing IDs must be titled "subjectIDdps". IDs must be
%               in consecutive order, starting with 1 and ending with
%               number of subjects. Column containing visit numbers must be
%               titled "visitdps". Within each subject, visit numbers must
%               be in consecutive order, starting with 1 and ending with
%               number of visits. See prepareDPS.m for preprocessing data
%               sets to satisfy these requirements.
% y             matrix containing biomarker measurements (one biomarker 
%               per column). Must have equal number of rows as da.
%               Rows of da and y must be in correspondance (i.e. must
%               correspond to the same subject and visit)
% dx_normal     diagnosis value in da corresponding to controls/normals
% tol           convergence tolerance for EM algorithm
% maxIterations maximum number of iterations for EM algorithm
% age_colname   name of column in da containing age information
% dx_colname    name of column in da containing diagnosis information
% model_nocorr  model fitted using fitLinearDPS_EM_nocorr
% coord         struct with fields X, Y, Z, each of which is a
%               numBiomarkers-by-1 vector containing the X, Y, Z
%               coordinates of the biomarkers
% rho_init      initial value for the spatial correlation parameter \rho
% maxDistance_subset only voxels with distancemaxDistance_subset or less
%                    to the center voxel will be used to estimate the 
%                    spatial correlation matrix
% isdiagonal_V  set to TRUE if the variance V of (\alpha_i,\beta_i) is
%               assumed to be diagonal (i.e. \alpha and \beta are
%               independent).
% correlationFun a function handle that takes (d,rho) as parameters, where
%                d is a vector of distances and rho is a scalar, and 
%                returns a vector of same size as d whose elements are 
%                between 0 and 1. Value returned at d=0 should be 1. The 
%                output corresponds to the correlation between two
%                biomarkers separated by the given distance d. This
%                function should give rise to a valid positive definite
%                correlation matrix. Alternatively, correlationFun can be
%                specified as a string:
%                'exponential'         exp(-d/rho)
%                'gaussian'            exp(-(d/rho)^2)
%                'rational quadratic'  1 / (1+(d/rho)^2)
%                'linear'              (1 - d/rho) * (d<rho)
%                'spherical'           (1-1.5*d/rho+0.5*(d/rho)^3)*(d<rho)
% fixrho        OPTIONAL: if true, spatial correlation parameter rho will 
%               not be updated (default: false)
% fitRhoLambdaTogether OPTIONAL: if true, rho and lambda will be
%                      jointly numerically optimized (default: true).
%                      Setting to false uses an approximate update equation
%                      for lambda and numerical optimization for rho.
% useNugget     OPTIONAL: If true, a nugget in the spatial correlation 
%               function is used to model a correlation value significantly
%               less than 1 at a distance close to 0.
% w             OPTIONAL: diagonal values of the fixed weight matrix W.
%               Default is a vector of ones, yielding identity matrix for W
% verbose       OPTIONAL: logical indicating whether info messages should
%               be displayed
%
% See also prepareDPS, fitLinearDPS_EM_nocorr.
%
%
% This file is part of Progression Score Model Toolkit.
%
% Progression Score Model Toolkit is distributed under the terms of GNU
% Lesser General Public License, version 3 (GPLv3).
%
% Progression Score Model Toolkit is free software: you can redistribute it
% and/or modify it under the terms of the GNU Lesser General Public License
% as published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% Progression Score Model Toolkit is distributed in the hope that it will
% be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.
%
% You should have received a copy of the GNU Lesser General Public License
% along with Progression Score Model Toolkit. If not, see
% <http://www.gnu.org/licenses>.
%
%
% Progression Score Model Toolkit was made by Murat Bilgel and is
% supported, in part, by the National Institute on Aging, a U.S. Federal
% Government research institute, while Murat Bilgel was a participant in
% the NIH Graduate Partnerships Program.
%
% For any code or method related questions, contact Murat Bilgel at
% mbilgel1@jhu.edu.
%

% Extract age and diagnosis data
age = double(da(:,age_colname));
dx = double(da(:,dx_colname));

numSubjects = length(unique(da.subjectIDdps));
numBiomarkers = size(y,2);
numVisitsPerSubject = accumarray(da.subjectIDdps,ones(length(da.subjectIDdps),1));
numSubjVisits = sum(numVisitsPerSubject);

% Initialize parameters and subject-specific variables using results of
% model where biomarkers were assumed to be independent
a = model_nocorr.a;
b = model_nocorr.b;
alpha = model_nocorr.alpha;
beta = model_nocorr.beta;
ubar = model_nocorr.ubar;
V = model_nocorr.V;

rho = rho_init;

if nargin<18
    verbose = true;
end

if nargin<17
    w = ones(numBiomarkers,1);
    lambda = mean(model_nocorr.lambda);
else
    lambda = 1;
end

if nargin<16
    useNugget = false;
else
    nugget = 1e-15;
end

if nargin<15
    fitRhoLambdaTogether = true;
end

if nargin<14
    fixrho = false;
elseif fixrho && fitRhoLambdaTogether
    warning('Since rho is fixed, setting fitRhoLambdaTogether flag to false');
    fitRhoLambdaTogether = false;
end

if ischar(correlationFun)
    switch correlationFun(1)
        case 'e'
            % Exponential
            correlationFun = @(d,rhop) exp(-d/rhop);
        case 'g'
            % Gaussian
            correlationFun = @(d,rhop) exp(-d.^2 / rhop^2);
        case 'r'
            % Rational quadratic
            correlationFun = @(d,rhop) 1./(1+(d/rhop).^2);
        case 'l'
            % Linear
            correlationFun = @(d,rhop) (1-d/rhop) .* (d<rhop);
        case 's'
            % Spherical
            correlationFun = @(d,rhop) (1-1.5*d/rhop+0.5*(d/rhop).^3) .* (d<rhop);
        otherwise
            error('Invalid correlation function specification. Select from (e)xponential, (g)aussian, (r)ational quadratic, (l)inear, or (s)pherical!');
    end
elseif ~isa(correlationFun,'function_handle')
    error('Invalid correlation function handle!');
end

% ysum is \sum_{i,j} \mathbf{y}_{ij}
ysum = sum(y,1)';
% ybar is \frac{1}{\sum_i v_i} \sum_{i,j} \mathbf{y}_{ij}
ybar = mean(y,1)';

%%
if ~isdiagonal_V
    % Prior covariance V is 2-by-2 and off-diagonals are allowed to be
    % non-zero: V is characterized by 3 parameters
    % use log Cholesky decomposition to parameterize V as described in
    % section 2.1.2
    uppertri_V = triu(true(2));
    diag_V = diag(true(2,1));
    
    U = chol(V);
    U(diag_V) = log(U(diag_V));
    
    logv = U(uppertri_V);
else
    % Prior covariance V is assumed to be diagonal. To ensure positivity,
    % we use log-parameterization
    logv = log(diag(V));
end

%%
function V_expectation = calculateExpectationV(logv)
    % this function is used to perform the arg max in Eq.15
    % only the terms that depend on V in Q are retained -- see Eq.A.13
    UU = zeros(2);
    UU(uppertri_V) = logv;
    UU(diag_V) = exp(UU(diag_V));
    VV = UU'*UU;

    % term2 is (\hat{\mathbf{u}}'_i - \mathbf{m})^T V^{-1} (\hat{\mathbf{u}}'_i - \mathbf{m})
    term2 = 0;
    % term3 is Tr(V^{-1} \Sigma'_i)
    term3 = 0;
    for i=1:numSubjects
        term2 = term2 + sum((UU'\([alpha(i); beta(i)]-ubar)).^2);
        term3 = term3 + Sigma_11(i)*VV(2,2) - 2*Sigma_12(i)*VV(1,2) + Sigma_22(i)*VV(1,1);
    end
    
    V_expectation = -0.5*numSubjects*log(det(VV)) -0.5*term2 -0.5*term3/det(VV);
end

%% Spatial information and covariance matrix  
ii = cell(numBiomarkers,1);
jj = cell(numBiomarkers,1);
ss = cell(numBiomarkers,1);
for id=1:numBiomarkers
    sqdists = (coord.X(id)-coord.X).^2 + (coord.Y(id)-coord.Y).^2 + ...
        (coord.Z(id)-coord.Z).^2;
    ii{id} = 1:numBiomarkers;
    jj{id} = id*ones(1,numBiomarkers);
    ss{id} = sqrt(sqdists(ii{id}))';
end
% No need to explicitly calculate distances, but here is the code to do it
%distances = sparse([ii{:}],[jj{:}],[ss{:}],numBiomarkers,numBiomarkers);

% Use voxels that are within a certain distance from the "center" voxel
% for spatial correlation estimation to reduce memory & time requirements
centerX = mean(coord.X);
centerY = mean(coord.Y);
centerZ = mean(coord.Z);
sqdists = (centerX-coord.X).^2 + (centerY-coord.Y).^2 + (centerZ-coord.Z).^2;
idx_subset = (sqdists < maxDistance_subset^2/4);
if sum(idx_subset)<3
    error('There are too few biomarkers that are within the selected maxDistance_subset threshold. Spatial correlations cannot be estimated. Increase the maxDistance_subset threshold');
end
display(['Will use ',num2str(sum(idx_subset)),' of the ',num2str(numBiomarkers),' biomarkers for spatial covariance estimation']);

correlationMatrix = @(rhop) sparse([ii{:}],[jj{:}],correlationFun([ss{:}],rhop),numBiomarkers,numBiomarkers);

if all(rho==0)
    C = speye(numBiomarkers);
else
    C = correlationMatrix(rho);
end
tQ = chol(C)';

tS = spdiags(lambda*w,0,numBiomarkers,numBiomarkers) * tQ;
tS_subset = spdiags(lambda*w(idx_subset),0,sum(idx_subset),sum(idx_subset)) * chol(C(idx_subset,idx_subset))';

clear C;

diag_C = diag(true(numBiomarkers,1));

%% Model fit with correlation
function expectation = calculateExpectationRho(logrho) 
    if (any(isinf(logrho)) || any(isnan(logrho)))
        rho = 0;
        C = speye(numBiomarkers);
    else
        rho = exp(logrho(1));

        if useNugget
            nugget = exp(logrho(2))/(exp(logrho(2))+1);
            C = (1-nugget)*correlationMatrix(rho);
            C(diag_C) = 1;
        else
            C = correlationMatrix(rho);
        end
    end
    
    Csubset = C(idx_subset,idx_subset);

    display(['Evaluating rho=',num2str(rho)]);
    tQ = chol(Csubset)';
    tS = spdiags(lambda*w(idx_subset),0,sum(idx_subset),sum(idx_subset)) * tQ;
    
    term1 = numSubjVisits*2*(sum(log(diag(tQ)))+sum(log(lambda*w(idx_subset))));
    
    term2 = 0;
    
    aRa_new = sum((tS \ a(idx_subset)).^2);
    
    for ij=1:numSubjVisits
        term2 = term2 + sum( ( tS \ (y(ij,idx_subset) - y_pred(ij,idx_subset))' ).^2 );
    end
    
    expectation = -0.5*(term1 + term2 + aRa_new*trace_term1_subset);
end

function expectation = calculateExpectationRhoLambda(logrholambda)
    logrho = logrholambda(1);
    loglambda = logrholambda(2);

    lambdaa = exp(loglambda);
    
    if (any(isinf(logrholambda)) || any(isnan(logrholambda)))
        rho = zeros(size(logrho));
        C = speye(numBiomarkers);
    else
        rho = exp(logrho);

        if useNugget
            lognugget = logrholambda(3);
            nugget = exp(lognugget)/(exp(lognugget)+1);
            C = (1-nugget)*correlationMatrix(rho);
            C(diag_C) = 1;
        else
            C = correlationMatrix(rho);
        end
    end
    
    Csubset = C(idx_subset,idx_subset);

    display(['Evaluating rho=',num2str(rho),' lambda=',num2str(lambdaa)]);
    tQ = chol(Csubset)';
    tS = spdiags(lambdaa*w(idx_subset),0,sum(idx_subset),sum(idx_subset)) * tQ;
    
    term1 = numSubjVisits*2*(sum(log(diag(tQ)))+sum(log(lambdaa*w(idx_subset))));
    
    term2 = 0;
    
    aRa_new = sum((tS \ a(idx_subset)).^2);

    for ij=1:numSubjVisits
        term2 = term2 + sum( ( tS \ (y(ij,idx_subset) - y_pred(ij,idx_subset))' ).^2 );
    end
    
    expectation = -0.5*(term1 + term2 + aRa_new*trace_term1_subset);
end

%%
itr = 1;
converged = 0;
loglik = -Inf;
loglik_list = zeros(maxIterations,1);
rho_list = zeros(maxIterations,length(rho));

if verbose
    tic
end
while ~converged && itr<=maxIterations
    loglik_old = loglik;
    
    a_old = a;
    b_old = b;
    lambda_old = lambda;
    rho_old = rho;
    ubar_old = ubar;
    V_old = V;
    alpha_old = alpha;
    beta_old = beta;
    
    % E-step: Calculate \alpha'_i, \beta'_i, s'_{ij}
    Sigma_11 = zeros(numSubjects,1);
    Sigma_12 = zeros(numSubjects,1);
    Sigma_22 = zeros(numSubjects,1);
    
    % trace_term1 corresponds to the term
    % \mathbf{q}_{ij}^T \Sigma'_i \mathbf{q}_{ij}
    % in Eqns 12 and 13
    trace_term1 = 0;
    trace_term1_subset = 0;
    detV = det(V);
    count = 1;
    inv_tS_a = tS \ a;
    aRa = sum(inv_tS_a.^2);
    aRa_subset = sum((tS_subset \ a(idx_subset)).^2);
    for i=1:numSubjects
        idx = da.subjectIDdps==i;
        agei = age(idx);
        
        % invSi_mui is equal to 
        % \sum_j Z'_{ij}^T R'^{-1} (\mathbf{y}_{ij} - \mathbf{b}')
        % in Eq.10
        invSi_mui = zeros(2,1);
        for j=1:numVisitsPerSubject(i)
            yij = y(count,:)';
            q = [age(count); 1];
            invSi_mui = invSi_mui + q * inv_tS_a' * (tS \ (yij-b));
            count = count+1;
        end
        
        % Covariance matrix \Sigma'_i as given below Eq.10
        Sigmai = [aRa*numVisitsPerSubject(i)+V(1,1)/detV -aRa*sum(agei)+V(1,2)/detV; ...
                  -aRa*sum(agei)+V(1,2)/detV aRa*sum(agei.^2)+V(2,2)/detV];
        Sigmai = Sigmai / det(Sigmai);
        
        Sigma_11(i) = Sigmai(1,1);
        Sigma_12(i) = Sigmai(1,2);
        Sigma_22(i) = Sigmai(2,2);
        
        trace_term1 = trace_term1 + Sigma_11(i)*sum(agei.^2) + 2*Sigma_12(i)*sum(agei) + Sigma_22(i)*numVisitsPerSubject(i);
        
        Sigmai_subset = [aRa_subset*numVisitsPerSubject(i)+V(1,1)/detV -aRa_subset*sum(agei)+V(1,2)/detV; ...
                  -aRa_subset*sum(agei)+V(1,2)/detV aRa_subset*sum(agei.^2)+V(2,2)/detV];
        Sigmai_subset = Sigmai_subset / det(Sigmai_subset);
        trace_term1_subset = trace_term1_subset + Sigmai_subset(1,1)*sum(agei.^2) + 2*Sigmai_subset(1,2)*sum(agei) + Sigmai_subset(2,2)*numVisitsPerSubject(i);
        
        % \hat{\mathbf{u}}'_i as given in Eq.10
        ui = Sigmai*(invSi_mui + V\ubar); % vector containing \alpha'_i and \beta'_i
        
        alpha(i) = ui(1);
        beta(i) = ui(2);
    end
    % Progression scores s'_{ij}
    dps = repelem(alpha,numVisitsPerSubject) .* age + repelem(beta,numVisitsPerSubject);
    
    % sum_s is \sum_{i,j} s'_{ij}
    sum_s = sum(dps);
    % sum_ssq is \sum_{i,j} s'_{ij}^2
    sum_ssq = sum(dps.^2);
    % sum_ys is \sum_{i,j} \mathbf{y}_{ij} s'_{ij}
    sum_ys = sum(bsxfun(@times,y,dps),1)';
    
    % M-step  
    a = (sum_ys - sum_s * ybar) / (sum_ssq + trace_term1 - sum_s^2 / numSubjVisits); % Eq.12
    b = ( ( sum_ssq + trace_term1 ) * ysum - sum_s * sum_ys) / ...
        (numSubjVisits * ( sum_ssq + trace_term1 ) - sum_s^2); % Eq.13
    
    y_pred = bsxfun(@plus,kron(a',dps),b');
    
    if verbose
        disp('Done with a b update');
    end
    
    ubar = [mean(alpha); mean(beta)];
    % Update V
    if isdiagonal_V
        V(1,1) = sum((alpha-ubar(1)).^2 + Sigma_11) / numSubjects;
        V(2,2) = sum((beta-ubar(2)).^2 + Sigma_22) / numSubjects;
        
        objval1 = -0.5*numSubjects*(log(V(1,1))+log(V(2,2))) ...
                  -0.5*sum((alpha-ubar(1)).^2 / V(1,1) + (beta-ubar(2)).^2 / V(2,2)) ...
                  -0.5*(sum(Sigma_11)/V(1,1)+sum(Sigma_22)/V(2,2));
        
        U = sqrt(V);
    else
        [logv,objval1] = fminsearch(@(x) -calculateExpectationV(x),logv); % Eq.15
        
        U = zeros(2);
        U(uppertri_V) = logv;
        U(diag_V) = exp(U(diag_V));

        V = U'*U;
    end
    
    if verbose
        disp('Done with m V update');
    end
    
    % Covariance parameters
    if fitRhoLambdaTogether % Eq.16
        options = optimset('MaxIter',20);
        if useNugget
            [logrholambda,objval2] = fminsearch(@(x) -calculateExpectationRhoLambda(x),[log(rho) log(lambda) log(nugget/(1-nugget))],options);
            nugget = exp(logrholambda(3))/(1+exp(logrholambda(3)));
        else
            [logrholambda,objval2] = fminsearch(@(x) -calculateExpectationRhoLambda(x),[log(rho) log(lambda)],options);
        end
        rho = exp(logrholambda(1));
        lambda = exp(logrholambda(2));
    else
        tmp = 0;
        wtQ = spdiags(w,0,numBiomarkers,numBiomarkers) * tQ;
        for ij=1:size(y,1)
            resid = y(ij,:)' - y_pred(ij,:)';
            tmp = tmp + sum(( wtQ \ resid).^2);
        end
        lambda = sqrt( ( tmp + trace_term1 * sum((wtQ\a).^2) ) / (numSubjVisits * numBiomarkers)); % single lambda
        
        if ~fixrho
            % may need to increase MaxIter param depending on the data set
            options = optimset('MaxIter',20);
            if useNugget
                [logrho,objval2] = fminsearch(@(x) -calculateExpectationRho(x),[log(rho) log(nugget/(1-nugget))],options);
                nugget = exp(logrho(2))/(1+exp(logrho(2)));
            else
                [logrho,objval2] = fminsearch(@(x) -calculateExpectationRho(x),log(rho),options);
            end
            rho = exp(logrho(1));
        end
    end
    
    % For the next iteration
    if ~fixrho
        if (any(rho<eps) || any(isnan(rho)))
            rho = 0;
            C = speye(numBiomarkers);
        else
            if useNugget
                C = (1-nugget)*correlationMatrix(rho);
                C(diag_C) = 1;
            else
                C = correlationMatrix(rho);
            end
        end

        tQ = chol(C)';
        tS_subset = spdiags(lambda*w(idx_subset),0,sum(idx_subset),sum(idx_subset)) * chol(C(idx_subset,idx_subset))';
        
        clear C;
    end
    
    tS = spdiags(lambda*w,0,numBiomarkers,numBiomarkers) * tQ;

    if verbose
        disp('Done with covariance update');
    end
 
    % Compute incomplete loglikelihood with the new parameters
    count = 1;
    detV = det(V);
    inv_tS_a = tS \ a;
    aRa = sum(inv_tS_a.^2);
    sum_yij_b = 0;
    sumlogdetSigmai = 0;
    sum_uiSigmaiui = 0;
    for i=1:numSubjects
        idx = da.subjectIDdps==i;
        agei = age(idx);

        invSi_mui = zeros(2,1);
        for j=1:numVisitsPerSubject(i)
            yij = y(count,:)';
            q = [age(count); 1];
            sum_yij_b = sum_yij_b + sum((tS \ (yij-b)).^2);
            invSi_mui = invSi_mui + q * inv_tS_a' * (tS \ (yij-b));
            count = count+1;
        end

        Sigmai = [aRa*numVisitsPerSubject(i)+V(1,1)/detV -aRa*sum(agei)+V(1,2)/detV; ...
                  -aRa*sum(agei)+V(1,2)/detV aRa*sum(agei.^2)+V(2,2)/detV];
        Sigmai = Sigmai / det(Sigmai);

        sumlogdetSigmai = sumlogdetSigmai + log(det(Sigmai));

        sum_uiSigmaiui = sum_uiSigmaiui + (invSi_mui + V\ubar)'*Sigmai*(invSi_mui + V\ubar);
    end
    
    % marginal loglikelihood as in Eq.A.17
    loglik = 0.5*( sumlogdetSigmai -numSubjects*log(detV) ...
                 -numSubjVisits*numBiomarkers*log(2*pi) ...
                 -numSubjVisits*2*sum(log(diag(tS))) ) ...
            -0.5*sum_yij_b ...
            -0.5*numSubjects*ubar'*(V\ubar) ...
            +0.5*sum_uiSigmaiui;
   
    loglik_list(itr) = loglik;
    rho_list(itr,:) = rho;

    if (loglik < loglik_old)
        disp(['Loglik decreased! ',num2str(loglik_old),' ',num2str(loglik)]);
        a = a_old;
        b = b_old;
        lambda = lambda_old;
        rho = rho_old;
        alpha = alpha_old;
        beta = beta_old;
        dps = repelem(alpha,numVisitsPerSubject) .* age + repelem(beta,numVisitsPerSubject);
        ubar = ubar_old;
        V = V_old;
        y_pred = bsxfun(@plus,kron(a',dps),b');
        loglik = loglik_old;
        converged = -1;
        loglik_list(itr) = loglik;
        rho_list(itr) = rho;
        break;
    end
    
    itr = itr+1;
    
    % Check for convergence
    if (abs(loglik/loglik_old - 1) < tol)
        converged = 1;
    end
    
    if verbose
        display([num2str(loglik_old),' ',num2str(loglik)]);
    end
end
if verbose
    toc
end

loglik_list(itr:end) = [];
rho_list(itr:end,:) = [];
if isempty(loglik_list)
    loglik_list = loglik;
    rho_list = rho_init;
end

%%
% Ensure that on average, PS increases with age
if ubar(1) < 0
    a = -a;
    alpha = -alpha;
    beta = -beta;
    dps = -dps;
    ubar = -ubar;
end

% Standardize PS as explained in section 2.4
mu = mean(dps(da.visitdps==1 & dx==dx_normal));
sdev = std(dps(da.visitdps==1 & dx==dx_normal));
alpha_scaled = alpha/sdev;
beta_scaled = (beta - mu)/sdev;
dps_scaled = repelem(alpha_scaled,numVisitsPerSubject) .* age + repelem(beta_scaled,numVisitsPerSubject);
a_scaled = sdev*a;
b_scaled = b + mu*a;
ubar_scaled = [ubar(1); ubar(2)-mu] / sdev;
V_scaled = V / sdev^2;

if isdiagonal_V
    nu_scaled = 0.5*log(diag(V_scaled));
else
    U = chol(V_scaled);
    U(diag_V) = log(U(diag_V));
    nu_scaled = U(uppertri_V);
end

posteriorVariance = zeros(2,2,numSubjects);
posteriorVariance(1,1,:) = Sigma_11 / sdev^2;
posteriorVariance(1,2,:) = Sigma_12 / sdev^2;
posteriorVariance(2,1,:) = Sigma_12 / sdev^2;
posteriorVariance(2,2,:) = Sigma_22 / sdev^2;

% Compute the variance of each PS value
dps_var = repelem(Sigma_11,numVisitsPerSubject).*age.^2 + ...
          2*repelem(Sigma_12,numVisitsPerSubject).*age + ...
          repelem(Sigma_22,numVisitsPerSubject);
dps_var_scaled = dps_var / sdev^2;

% Parameter estimates
model.a = a_scaled;
model.b = b_scaled;
model.lambda = lambda;
model.rho = rho;
model.ubar = ubar_scaled;
model.V = V_scaled;
model.nu = nu_scaled;

% Values of \alpha and \beta that maximize the posterior
model.alpha = alpha_scaled;
model.beta = beta_scaled;
model.dps = dps_scaled;

% Predicted biomarker values (same dimension as y)
model.y_pred = y_pred;

model.loglik = loglik_list;

model.numIterations = itr-1;
model.converged = converged;

model.rho_list = rho_list;

model.w = w;
if useNugget
    model.nugget = nugget;
else
    model.nugget = NaN;
end

model.numParams = 3*numBiomarkers + 1 + 1 + 2 + 2*isdiagonal_V + 3*(~isdiagonal_V) + 1*useNugget;
model.aic = 2*model.numParams - 2*loglik;

model.posteriorVariance = posteriorVariance;
model.dps_var = dps_var_scaled;

model.inputParams.dx_normal = dx_normal;
model.inputParams.tol = tol;
model.inputParams.maxIterations = maxIterations;
model.inputParams.age_colname = age_colname;
model.inputParams.dx_colname = dx_colname;
model.inputParams.model_nocorr = model_nocorr;
model.inputParams.coord = coord;
model.inputParams.rho_init = rho_init;
model.inputParams.maxDistance_subset = maxDistance_subset;
model.inputParams.isdiagonal_V = isdiagonal_V;
model.inputParams.fixrho = fixrho;
model.inputParams.fitRhoLambdaTogether = fitRhoLambdaTogether;
model.inputParams.correlationFun = correlationFun;
model.inputParams.w = w;
model.inputParams.useNugget = useNugget;

end