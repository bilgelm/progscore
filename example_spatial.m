% example_spatial.m
%
% This file is part of Progression Score Model Toolkit.
%
% Progression Score Model Toolkit is distributed under the terms of GNU
% Lesser General Public License, version 3 (GPLv3).
%
% Progression Score Model Toolkit is free software: you can redistribute it
% and/or modify it under the terms of the GNU Lesser General Public License
% as published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% Progression Score Model Toolkit is distributed in the hope that it will
% be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.
%
% You should have received a copy of the GNU Lesser General Public License
% along with Progression Score Model Toolkit. If not, see
% <http://www.gnu.org/licenses>.
%
%
% Progression Score Model Toolkit was made by Murat Bilgel and is
% supported, in part, by the National Institute on Aging, a U.S. Federal
% Government research institute, while Murat Bilgel was a participant in
% the NIH Graduate Partnerships Program.
%
% For any code or method related questions, contact Murat Bilgel at
% mbilgel1@jhu.edu.
%

%% SIMULATE DATA
% Generate data based on progression score model
maxNumVisits = 7;

imdim = [5 5 5];
voxSize = [4 4 4];

numBiomarkers = prod(imdim);
numSubjects = 100;
numVisitsPerSubject = randsample(1:maxNumVisits,numSubjects,true);

numSubjVisits = sum(numVisitsPerSubject);
maxDistance = 100;

% Ground truth subject-specific variables
ubar_truth = [.05;-3.80];
V_truth = [0.0055 -0.4; -0.4 30]; % make sure that det(V_truth) is positive!
u_truth = mvnrnd(ubar_truth,V_truth,numSubjects);
alpha_truth = u_truth(:,1);
beta_truth = u_truth(:,2);

% Ground truth trajectory parameters
a_truth = raylrnd(.09,numBiomarkers,1)-0.05;
b_truth = normrnd(1.1,0.15,numBiomarkers,1);

% Generate age at baseline
age_baseline = unifrnd(56,93,numSubjects,1);

% Subject IDs
subject = repelem(1:numSubjects,numVisitsPerSubject)';
% All subjects are normals
dx = ones(length(subject),1);

% Generate visit numbers and ages at follow-up visits
visit = zeros(length(subject),1);
age = zeros(length(subject),1);
for i=1:numSubjects
    idx = subject==i;
    vi = numVisitsPerSubject(i);
    visit(idx) = 1:vi;
    intervals = unifrnd(1,3,vi-1,1);
    age(idx) = cumsum([age_baseline(i);intervals]);
end

% Compute ground truth DPS values
dps_truth = repelem(alpha_truth,numVisitsPerSubject) .* age + repelem(beta_truth,numVisitsPerSubject);

% Normalize DPS so that mean is 0 and stdev is 1 at baseline
mu = mean(dps_truth(visit==1));
sdev = std(dps_truth(visit==1));
alpha_truth = alpha_truth/sdev;
beta_truth = (beta_truth - mu)/sdev;
ubar_truth(1) = ubar_truth(1)/sdev;
ubar_truth(2) = (ubar_truth(2) - mu)/sdev;
V_truth = V_truth / sdev^2;
dps_truth = repelem(alpha_truth,numVisitsPerSubject) .* age + repelem(beta_truth,numVisitsPerSubject);

y_truth = bsxfun(@plus,kron(a_truth',dps_truth),b_truth');

% Noise
lambda_truth = raylrnd(0.04,numBiomarkers,1)+0.08;
rho_truth = 4.7;

% Spatial correlation function to use
% exponential
%correlationFun_truth = @(d,rhop) exp(-d/rhop);
% gaussian
%correlationFun_truth = @(d,rhop) exp(-d.^2/rhop^2);
% rational quadratic
correlationFun_truth = @(d,rhop) 1./(1+(d/rhop).^2);

% Spatial information
[coordX,coordY,coordZ] = meshgrid(0:voxSize(2):voxSize(2)*(imdim(2)-1), ...
                                  0:voxSize(1):voxSize(1)*(imdim(1)-1), ...
                                  0:voxSize(3):voxSize(3)*(imdim(3)-1));

coord.X = coordX(:);
coord.Y = coordY(:);
coord.Z = coordZ(:);

ii = cell(numBiomarkers,1);
jj = cell(numBiomarkers,1);
ss = cell(numBiomarkers,1);
for id=1:numBiomarkers
    sqdists = (coord.X(id)-coord.X).^2 + (coord.Y(id)-coord.Y).^2 + ...
              (coord.Z(id)-coord.Z).^2;
    ii{id} = 1:numBiomarkers;
    jj{id} = id*ones(1,numBiomarkers);
    ss{id} = sqrt(sqdists(ii{id}))';
end

if all(rho_truth==0)
    C_truth = speye(numBiomarkers);
else
    C_truth = sparse([ii{:}],[jj{:}],correlationFun_truth([ss{:}],rho_truth),numBiomarkers,numBiomarkers);
end

R_truth = spdiags(lambda_truth,0,numBiomarkers,numBiomarkers) * C_truth * spdiags(lambda_truth,0,numBiomarkers,numBiomarkers);

y = y_truth + mvnrnd(zeros(numBiomarkers,1),R_truth,numSubjVisits);

da = dataset({subject,'subjectIDdps'},{visit,'visitdps'},{age,'age'},{dx,'dx'});

%figure, plot(y,y_truth,'.')

%% First, fit model without correlations
age_colname = 'age';
dx_colname = 'dx';
dx_normal = 1;
tol = 1e-6;
maxIterations = 100;
doMultiLambda = true;
isdiagonal_V = false;

model_nocorr = fitLinearDPS_EM_nocorr(da,y,dx_normal,tol,maxIterations,age_colname,dx_colname,doMultiLambda,isdiagonal_V);

figure, plot(a_truth,model_nocorr.a,'.'); xlabel('True trajectory slope (a)'); ylabel('Estimated trajectory slope (a)'); title('Result of no correlation model');
figure, plot(dps_truth,model_nocorr.dps,'.'); xlabel('True PS'); ylabel('Predicted PS'); title('Result of no correlation model');

%% We first do an exploration of the semivariogram to understand the spatial correlations
residIm = bsxfun(@times,y - model_nocorr.y_pred,1./model_nocorr.lambda');
dist_cutoff = 30;
nbins = 10;

semivariogram = exploreSemivariogram(residIm,coord,dist_cutoff,nbins);
%exclude first element from semivariogram
semivariogram.x = semivariogram.x(2:end);
semivariogram.y = semivariogram.y(2:end);
semivariogram.n = semivariogram.n(2:end);

%% Next, fit model with correlations
%rho_init = .1; % do not initialize at 0
maxDistance_subset = 99999;
fixrho = false;
fitRhoLambdaTogether = true; % setting this to false will yield an approximate solution, but it might converge faster
useNugget = false;
w = model_nocorr.lambda;

% we obtain an initialization for rho based on the semivariogram
correlationFun = 'rational quadratic';
figure, plot(semivariogram.x,semivariogram.y,'.-');
% showfit and makevarfit are from the ezyfit package
showfit('f(x)=1-1/(1+(x/rho_init)^2)');
makevarfit;

model_rq = fitLinearDPS_EM_spatial(da,y,dx_normal,...
                 tol,maxIterations,age_colname,dx_colname,...
                 model_nocorr,coord,rho_init,...
                 maxDistance_subset,isdiagonal_V,correlationFun,...
                 fixrho,fitRhoLambdaTogether,useNugget,w);

figure, plot(a_truth,model_rq.a,'.'); xlabel('True trajectory slope (a)'); ylabel('Estimated trajectory slope (a)'); title(['Result of ',correlationFun,' correlation model']);
figure, plot(dps_truth,model_rq.dps,'.'); xlabel('True PS'); ylabel('Predicted PS'); title(['Result of ',correlationFun,' correlation model']);

%% We can try models with different spatial correlation functions to see which one fits better
% This approach can be used for model selection when we do not know which
% spatial correlation function to use
correlationFun = 'exponential';
figure, plot(semivariogram.x,semivariogram.y,'.-');
showfit('f(x)=1-exp(-x/rho_init)');
makevarfit;
model_exp = fitLinearDPS_EM_spatial(da,y,dx_normal,...
                 tol,maxIterations,age_colname,dx_colname,...
                 model_nocorr,coord,rho_init,...
                 maxDistance_subset,isdiagonal_V,correlationFun,...
                 fixrho,fitRhoLambdaTogether,useNugget,w);

correlationFun = 'gaussian';
figure, plot(semivariogram.x,semivariogram.y,'.-');
showfit('f(x)=1-exp(-x^2/rho_init^2)');
makevarfit;
model_gauss = fitLinearDPS_EM_spatial(da,y,dx_normal,...
                 tol,maxIterations,age_colname,dx_colname,...
                 model_nocorr,coord,rho_init,...
                 maxDistance_subset,isdiagonal_V,correlationFun,...
                 fixrho,fitRhoLambdaTogether,useNugget,w);

correlationFun = 'linear';
% ezyfit has a problem fitting this function, so I just picked an initial
% value that works
rho_init = 3;
model_linear = fitLinearDPS_EM_spatial(da,y,dx_normal,...
                 tol,maxIterations,age_colname,dx_colname,...
                 model_nocorr,coord,rho_init,...
                 maxDistance_subset,isdiagonal_V,correlationFun,...
                 fixrho,fitRhoLambdaTogether,useNugget,w);
             
correlationFun = 'spherical';
% ezyfit has a problem fitting this function, so I just picked an initial
% value
rho_init = 4;
model_sph = fitLinearDPS_EM_spatial(da,y,dx_normal,...
                 tol,maxIterations,age_colname,dx_colname,...
                 model_nocorr,coord,rho_init,...
                 maxDistance_subset,isdiagonal_V,correlationFun,...
                 fixrho,fitRhoLambdaTogether,useNugget,w);

[min_aic,min_id] = min([model_rq.aic,model_exp.aic,model_gauss.aic,model_linear.aic,model_sph.aic]);
correlationFuns = {'rational quadratic','exponential','gaussian','linear','spherical'};
disp([correlationFuns{min_id},' correlation function has the lowest AIC = ',num2str(min_aic)]);