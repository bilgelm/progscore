function [da_orig,y,origsubjlist,isbaseline] = prepareDPS(da_orig,...
         subject_colname,visit_colname,...
         age_colname,dx_colname,biomarker_colnames,...
         removeSingleVisits,maxNumBiomarkersMissing,covariate_colnames)
% prepareDPS        Read in data set and prepare for analysis with
%                   progression score model
%
% INPUTS:
% da_orig           dataset containing ID, visit number, age, diagnosis,
%                   biomarker measurements
% subject_colname   name of column in da_orig containing subject IDs
% visit_colname     name of column in da_orig containing visit numbers
% age_colname       name of column in da_orig containing age information
% dx_colname        name of column in da_orig containing diagnosis information
% biomarker_colnames names of columns in da_orig containing biomarker
%                    measurements
% removeSingleVisits OPTIONAL: TRUE/FALSE indicating whether single visits 
%                    should beremoved (default=FALSE)
% maxNumBiomarkerMissing OPTIONAL: Set to 0 if no missing values are allowed
%                        Defaults to numBiomarkers-1, meaning that visits
%                        will be retained unless all biomarkers are missing
% covariate_colnames OPTIONAL: name of column(s) in da_orig containing
%                    covariates. These are not used in model fitting but
%                    might be of interest for a post-analysis.
%
% OUTPUTS:
% da_orig           cleaned dataset
% y                 matrix containing biomarker measurements
% origsubjlist      list of original subject IDs
% isbaseline        vector of size equal to the number of rows of da_orig,
%                   indicating baseline visits
%
%
% This file is part of Progression Score Model Toolkit.
%
% Progression Score Model Toolkit is distributed under the terms of GNU
% Lesser General Public License, version 3 (GPLv3).
%
% Progression Score Model Toolkit is free software: you can redistribute it
% and/or modify it under the terms of the GNU Lesser General Public License
% as published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% Progression Score Model Toolkit is distributed in the hope that it will
% be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.
%
% You should have received a copy of the GNU Lesser General Public License
% along with Progression Score Model Toolkit. If not, see
% <http://www.gnu.org/licenses>.
%
%
% Progression Score Model Toolkit was made by Murat Bilgel and is
% supported, in part, by the National Institute on Aging, a U.S. Federal
% Government research institute, while Murat Bilgel was a participant in
% the NIH Graduate Partnerships Program.
%
% For any code or method related questions, contact Murat Bilgel at
% mbilgel1@jhu.edu.
%


% Check that all necessary columns exist in dataset
colnames = get(da_orig,'VarNames');
assert(strmatch(subject_colname,colnames) && ...
       strmatch(visit_colname,colnames) && ...
       strmatch(age_colname,colnames) && ...
       strmatch(dx_colname,colnames));

if strmatch('subjectIDdps',colnames)
    error('Please rename the subjectIDdps column to something else before running this code.');
end
if strmatch('visitdps',colnames)
    error('Please rename the visitdps column to something else before running this code.');
end

if nargin<7
    removeSingleVisits = false;
end

if nargin<8
    % No visit will be removed (unless all biomarker measures are missing)
    maxNumBiomarkersMissing = length(biomarker_colnames)-1;
else
    % Set maxNumBiomarkers to 0 to remove visits with any missing values
    assert(maxNumBiomarkersMissing>=0 && maxNumBiomarkersMissing<=length(biomarker_colnames)-1);
end

if nargin<9
    covariate_colnames = [];
end

% Retain necessary columns only
y = da_orig(:,biomarker_colnames);
da_orig = da_orig(:,[{subject_colname,visit_colname,age_colname,dx_colname},covariate_colnames]);

% Retain only the visits with subject identifier and age
ix = ismissing(da_orig(:,1:3),'StringTreatAsMissing',{'NaN','.','NA'});
da_orig = da_orig(~any(ix,2),:);
y = y(~any(ix,2),:);

y = double(y);

% Must have at least a given number of biomarker per individual
idx = sum(isnan(y),2)>maxNumBiomarkersMissing;
y = y(~idx,:);
da_orig = da_orig(~idx,:);

% Order data by subject and visit
da_orig = sortrows(da_orig,{subject_colname,visit_colname},'ascend');

% Remove subjects with single visit
if removeSingleVisits
    tbl = grpstats(da_orig(:,subject_colname),subject_colname);
    numVisitsBySubject = tbl.GroupCount;
    subjToRemove = double(tbl(numVisitsBySubject<2,subject_colname));
    for i=1:length(subjToRemove)
        idx_to_remove = double(da_orig(:,subject_colname))==subjToRemove(i);
        da_orig(idx_to_remove,:) = [];
        y(idx_to_remove,:) = [];
    end
end

% Rename subjects and visits with consecutive integers to facilitate matrix
% indexing later
origsubjlist = unique(double(da_orig(:,subject_colname)));
numSubjects = length(origsubjlist);
isbaseline = false(size(da_orig,1),1);
da_orig.subjectIDdps = 0;
da_orig.visitdps = 0;
for s=1:numSubjects
    idx = double(da_orig(:,subject_colname))==origsubjlist(s);
    da_orig.subjectIDdps(idx) = s;
    isbaseline(find(idx,1)) = true;
    
    numVisit = length(da_orig(idx,visit_colname));
    numUniqueVisit = length(unique(da_orig(idx,visit_colname)));
    if numVisit~=numUniqueVisit
        error(['Visit numbers must be unique within subjects! Subject ',num2str(s),' violated this requirement!']);
    end
    
    % Rename visits with consecutive integers
    da_orig.visitdps(idx) = 1:numVisit;
end