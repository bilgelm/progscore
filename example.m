% example.m
%
% This file is part of Progression Score Model Toolkit.
%
% Progression Score Model Toolkit is distributed under the terms of GNU
% Lesser General Public License, version 3 (GPLv3).
%
% Progression Score Model Toolkit is free software: you can redistribute it
% and/or modify it under the terms of the GNU Lesser General Public License
% as published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% Progression Score Model Toolkit is distributed in the hope that it will
% be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.
%
% You should have received a copy of the GNU Lesser General Public License
% along with Progression Score Model Toolkit. If not, see
% <http://www.gnu.org/licenses>.
%
%
% Progression Score Model Toolkit was made by Murat Bilgel and is
% supported, in part, by the National Institute on Aging, a U.S. Federal
% Government research institute, while Murat Bilgel was a participant in
% the NIH Graduate Partnerships Program.
%
% For any code or method related questions, contact Murat Bilgel at
% mbilgel1@jhu.edu.
%

%% SIMULATE DATA
% Generate data based on progression score model
maxNumVisits = 7;

numBiomarkers = 80;
numSubjects = 100;
numVisitsPerSubject = randsample(1:maxNumVisits,numSubjects,true);

numSubjVisits = sum(numVisitsPerSubject);
maxDistance = 100;

% Ground truth subject-specific variables
ubar_truth = [.05;-3.80];
V_truth = [0.0055 -0.4; -0.4 30]; % make sure that det(V_truth) is positive!
u_truth = mvnrnd(ubar_truth,V_truth,numSubjects);
alpha_truth = u_truth(:,1);
beta_truth = u_truth(:,2);

% Ground truth trajectory parameters
a_truth = raylrnd(.09,numBiomarkers,1)-0.05;
b_truth = normrnd(1.1,0.15,numBiomarkers,1);

% Generate age at baseline
age_baseline = unifrnd(56,93,numSubjects,1);

% Subject IDs
subject = repelem(1:numSubjects,numVisitsPerSubject)';
% All subjects are normals
dx = ones(length(subject),1);

% Generate visit numbers and ages at follow-up visits
visit = zeros(length(subject),1);
age = zeros(length(subject),1);
for i=1:numSubjects
    idx = subject==i;
    vi = numVisitsPerSubject(i);
    visit(idx) = 1:vi;
    intervals = unifrnd(1,3,vi-1,1);
    age(idx) = cumsum([age_baseline(i);intervals]);
end

% Compute ground truth DPS values
dps_truth = repelem(alpha_truth,numVisitsPerSubject) .* age + repelem(beta_truth,numVisitsPerSubject);

% Normalize DPS so that mean is 0 and stdev is 1 at baseline
mu = mean(dps_truth(visit==1));
sdev = std(dps_truth(visit==1));
alpha_truth = alpha_truth/sdev;
beta_truth = (beta_truth - mu)/sdev;
ubar_truth(1) = ubar_truth(1)/sdev;
ubar_truth(2) = (ubar_truth(2) - mu)/sdev;
V_truth = V_truth / sdev^2;
dps_truth = repelem(alpha_truth,numVisitsPerSubject) .* age + repelem(beta_truth,numVisitsPerSubject);

y_truth = bsxfun(@plus,kron(a_truth',dps_truth),b_truth');

% Noise
lambda_truth = raylrnd(0.04,numBiomarkers,1)+0.08;

C = eye(numBiomarkers); % assume no correlation among biomarker residuals
R = spdiags(lambda_truth,0,numBiomarkers,numBiomarkers) * C * spdiags(lambda_truth,0,numBiomarkers,numBiomarkers);

y = y_truth + mvnrnd(zeros(numBiomarkers,1),R,numSubjVisits);

da = dataset({subject,'subjectIDdps'},{visit,'visitdps'},{age,'age'},{dx,'dx'});

%figure, plot(y,y_truth,'.')

model_truth.a = a_truth;
model_truth.b = b_truth;
model_truth.lambda = lambda_truth;
model_truth.ubar = ubar_truth;
model_truth.V = V_truth;
model_truth.alpha = alpha_truth;
model_truth.beta = beta_truth;
model_truth.dps = dps_truth;

%% Fit model without correlations
age_colname = 'age';
dx_colname = 'dx';
dx_normal = 1;
tol = 1e-6;
maxIterations = 100;
doMultiLambda = true;
doDiagonalV = false;

model_nocorr = fitLinearDPS_EM_nocorr(da,y,dx_normal,tol,maxIterations,age_colname,dx_colname,doMultiLambda,doDiagonalV);

%% ACTUAL DATA
% CHANGE THIS TO THE APPROPRIATE PATH FOR YOUR DATASET
csvfile = 'example.csv';

% CHANGE THESE TO THE APPROPRIATE COLUMN HEADER VALUES IN YOUR DATA
subject_colname = 'id';
visit_colname = 'vi';
age_colname = 'age';
dx_colname = 'diagnosis';
biomarker_colnames = {'bio1','bio2','bio3','bio4'}; % biomarkers to be included in model

% CHANGE THIS TO THE APPROPRIATE VALUE IN YOUR DATA
dx_normal = 1; % value of diagnosis that corresponds to normal individuals in data

% Read in data
da_orig = dataset('file',csvfile,'Delimiter',',');
removeSingleVisits = false;
removeMissing = true;
[da,y,origsubjlist,isbaseline] = ...
    prepareDPS(da_orig,subject_colname,visit_colname,age_colname,dx_colname,biomarker_colnames,removeSingleVisits,removeMissing);

% Fitting options
maxIterations = 100;
doMultiLambda = true;
isdiagonal_V = false;

model_nocorr = fitLinearDPS_EM_nocorr(da,y,dx_normal,tol,maxIterations,age_colname,dx_colname,doMultiLambda,isdiagonal_V);